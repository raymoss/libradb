from time import sleep
from picamera import PiCamera

camera = PiCamera(0)
#~ camera.resolution = (1024, 768)
camera.start_preview()
# Camera warm-up time
sleep(300)
camera.stop_preview()
#~ camera.capture('foo.jpg', resize=(320, 240))
camera.close()

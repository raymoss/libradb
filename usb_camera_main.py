import numpy as np
import cv2
import datetime
import time

FRAME_WIDTH = 1280
FRAME_HEIGHT = 960

def capture_image(cap):
    try:
        ret , frame = cap.read()
        if ret:
            output_file = "capture" + str(datetime.datetime.fromtimestamp(time.time()).strftime('_%Y_%m_%d_%H_%M_%S')) + ".jpg"
            cv2.imwrite(output_file, frame, [int(cv2.IMWRITE_JPEG_QUALITY),90])
    except:
        print "Could not capture image"

def capture_video(cap):
    output_file = "video_output" + str(datetime.datetime.fromtimestamp(time.time()).strftime('_%Y_%m_%d_%H_%M_%S')) + ".avi"
    out = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'XVID'), 20.0, (FRAME_WIDTH, FRAME_HEIGHT))
    while(True):
        ret, frame = cap.read()
        if ret:
            out.write(frame)
            cv2.imshow('frame',frame)
        else:
            print "Error capturing image"
        if cv2.waitKey(1) & 0xFF == ord('s') :
            break
    print "Stoping capture"
    out.release()

def main():
    cap = cv2.VideoCapture(1)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)
    #print cap.get(CV_CAP_PROP_FPS)
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        cv2.imshow('frame',frame)
        
        #~ if(cv2.waitKey(1) & 0xFF == ord('i')):
            
        
        #~ if(cv2.waitKey(1) & 0xFF == ord('v')):
            #~ print "Capturing video"
            #~ capture_video(cap)
            
        switch = cv2.waitKey(1) & 0xFF
        if switch == ord('q'):
            break
        elif switch == ord('i'):
            print "Capturing images"
            capture_image(cap)
        elif switch == ord('v'):
            print "Capturing video"
            capture_video(cap)
            
    cap.release()
    cv2.destroyAllWindows()
    
if __name__ == "__main__":
    main()

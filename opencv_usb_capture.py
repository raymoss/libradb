import numpy as np
import cv2

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
print cap.get(cv2.CAP_PROP_FRAME_WIDTH)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
print cap.get(cv2.CAP_PROP_FRAME_WIDTH)

#print cap.get(cv2.cv.CV_CAP_PROP_FPS)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    #cv2.imwrite("capture1.jpg", frame, [int(cv2.cv.CV_IMWRITE_JPEG_QUALITY),90])
    cv2.imshow('frame',frame)
    cv2.imwrite("capture2.jpg", frame, [int(cv2.IMWRITE_JPEG_QUALITY),90])
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
